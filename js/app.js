import React from 'react';
import {Router, Route, IndexRoute, hashHistory} from 'react-router';
import {render}  from 'react-dom';

//custom-components
import Main from './components/Main.react';
import Homepage from './components/Homepage.react';

class NoMatch extends React.Component {
    render(){
        return (
            <h2>Page not found</h2>
        );
    }
};

var routes = (
    <Route path="/" component={Main}>
        <IndexRoute component={Homepage}/>
        <Route path="*" component={NoMatch}/>
    </Route>
);

render(<Router history={hashHistory}>{routes}</Router>,document.getElementById('render-react'));