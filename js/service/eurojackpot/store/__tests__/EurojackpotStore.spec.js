var expect = require('chai').expect;

var Constants = require('../../Constants');
var Dispatcher = require('../../../dispatcher/Dispatcher');

var EurojackpotStoreModule = require('../EurojackpotStore.js');
var EurojackpotStore = EurojackpotStoreModule.EurojackpotStore;

function clearStoreState () {
    EurojackpotStore.clearStore();
}

beforeEach(clearStoreState);

describe("EurojackpotStore.spec.js", function(){

    describe("getCurrentResults()", function(){
        it("should return undefined if the store is empty", function(){

            expect(EurojackpotStore.getCurrentResults()).to.be.undefined;

        });

        it("should return the results saved in the store", function(){

            var results = {
                last: {
                    euroNumbers: [2,3],
                    numbers: [1,2,3,4],
                    currency: 'EUR'
                },
                next: {
                    nr: 121
                }};

            Dispatcher.dispatch({
                actionType: Constants.STORE_EJACKPOT_RESULTS,
                payload: results
            });

            expect(EurojackpotStore.getCurrentResults()).to.deep.equal(results.last);

        });

    });

});