import Dispatcher from '../../dispatcher/Dispatcher.js';
import {EventEmitter} from 'events';
import Constants from '../Constants';
import assign from 'object-assign';

const CHANGE_EVENT = 'change';

let _state = undefined;

const getState = () => {
    _state = _state || {};
    return _state;
};

let EurojackpotStore = assign({}, EventEmitter.prototype, {
    getCurrentResults(){
        return getState().currentResults;
    },
    clearStore(){
        _state = undefined;
    },
    emitChange() {
        this.emit(CHANGE_EVENT);
    },
    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
});

Dispatcher.register(function(action) {

    switch(action.actionType) {
        case Constants.STORE_EJACKPOT_RESULTS:

            getState().currentResults = action.payload.last;
            getState().nextJackpot = action.payload.next;

            EurojackpotStore.emitChange();
            break;
        default:
    }
});

const tiers = ["5 Numbers, 2 Euronumber", "5 Numbers, 1 Euronumber", "5 Numbers, 0 Euronumber",
    "4 Numbers, 2 Euronumber", "4 Numbers, 1 Euronumber", "4 Numbers, 0 Euronumber", "3 Numbers, 2 Euronumber",
    "2 Numbers, 2 Euronumber", "3 Numbers, 1 Euronumber", "3 Numbers, 0 Euronumber", "1 Numbers, 2 Euronumber",
    "2 Numbers, 1 Euronumber"];

module.exports = {
    EurojackpotStore: EurojackpotStore,
    tiers: tiers
};