import Dispatcher from '../../dispatcher/Dispatcher.js';
import Q from 'q';

import Constants from '../Constants.js';
import {get} from '../../../util/rest.js';
import {Action} from '../../../util/Action.js';

export function getEurojackpotResults(){
    return Q.when(get('https://media.lottoland.com/api/drawings/euroJackpot')).then(data => {
        Dispatcher.dispatch(new Action(Constants.STORE_EJACKPOT_RESULTS, data));
    });
}