import React from 'react';
import {Row, Col} from 'react-bootstrap';
import numeral from 'numeral';

import {tiers} from '../../service/eurojackpot/store/EurojackpotStore.js';

//function downloaded from http://stackoverflow.com/questions/9083037/convert-a-number-into-a-roman-numeral-in-javascript
function romanize(num) {
    var lookup = {M:1000,CM:900,D:500,CD:400,C:100,XC:90,L:50,XL:40,X:10,IX:9,V:5,IV:4,I:1},roman = '',i;
    for ( i in lookup ) {
        while ( num >= lookup[i] ) {
            roman += i;
            num -= lookup[i];
        }
    }
    return roman;
}

class ResultRow extends React.Component {

    render (){

        let odd = this.props.odd;
        let tier = `Tier ${romanize(odd.tier)}`
        let winners = `${odd.winners}x`;

        let prize = odd.prize.toString();
        prize = prize.substr(0, prize.length -2) + "." + prize.substr(prize.length -2);
        prize = "€" + numeral(prize).format('0,0.00');

        return (
            <Row>
                <Col md={2} xs={2}>
                    <span className="tier">{tier}</span>
                </Col>
                <Col md={4} xs={4}>
                    <span className="numbers">{tiers[odd.tier - 1]}</span>
                </Col>
                <Col md={2} xs={2}>
                    <span className="times">{winners}</span>
                </Col>
                <Col md={4} xs={4}>
                    <span className="prize">{prize}</span>
                </Col>
            </Row>
        );
    }
};

export default ResultRow