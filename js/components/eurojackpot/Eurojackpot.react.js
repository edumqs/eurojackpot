import React from 'react';
import {Row, Col, Panel} from 'react-bootstrap';
import _ from 'underscore';
import moment from 'moment';

import '../../../styles/eurojackpot.less';

import {getEurojackpotResults} from '../../service/eurojackpot/actions/EurojackpotActions';
import {EurojackpotStore} from '../../service/eurojackpot/store/EurojackpotStore';

import ResultRow from './ResultRow.react';

class Eurojackpot extends React.Component {
    constructor() {
        super();
        this._onDataChange = this._onDataChange.bind(this);
        this._renderResultBalls = this._renderResultBalls.bind(this);
        this._renderResultRows = this._renderResultRows.bind(this);
        this.state = {results: undefined};
    }

    componentDidMount() {
        EurojackpotStore.addChangeListener(this._onDataChange);
        getEurojackpotResults();
    }

    componentWillUnmount() {
        EurojackpotStore.removeChangeListener(this._onDataChange);
    }

    _onDataChange() {
        this.setState({
            results: EurojackpotStore.getCurrentResults()
        });
    }

    _renderResultBalls() {
        let numbers = _.map(this.state.results.numbers, (number, i) => {
            return <li key={"number_" + number}>{number}</li>;
        });

        let euroNumbers = _.map(this.state.results.euroNumbers, (euronumber, i) => {
            return <li key={"euronumber_" + euronumber} className="euro-number">{euronumber}</li>;
        });

        return [...numbers, ...euroNumbers] ;
    }

    _renderResultRows() {
        let resultOdds = this.state.results.odds;
        let odds = [];

        for (var key in resultOdds) {
            if(key == "rank0")
                continue;

            let odd = resultOdds[key];

            odd.tier = parseInt(key.substring(4));
            odds.push(odd);
        }

        return _.map(_.sortBy(odds, (o) => { return o.tier; }), (odd, i) => {
            return <ResultRow key={"odds_" + i} odd={odd} date />
        });
    }

    render () {

        if(this.state.results === undefined){
            return <span className="loading-data">Loading...</span>
        }

        let date = this.state.results.closingDate.substring(0,10);
        date = moment(date, 'DD.MM.YYYY').format('DD MMM YYYY');

        return (
            <div className="eurojackpot">
                <Row>
                    <Col sm={12}>
                        <div className="eurojackpot-header">
                            <span className="highlighted">Eurojackpot</span> Results & Winning Numbers
                        </div>
                    </Col>
                </Row>
                <div className="results">
                    <h2>EuroJackpot results for {date}</h2>
                    <ul className="result-balls">
                        {this._renderResultBalls()}
                    </ul>
                    <div className="result-rows">
                        {this._renderResultRows()}
                    </div>
                </div>
            </div>
        );
    }
};

export default Eurojackpot