import React from 'react';
import {Row, Col, Panel} from 'react-bootstrap';

import '../../styles/global.less';

class Main extends React.Component {

    render (){

        return (
            <div className="top-component">
                <div className="header">
                </div>
                <Row>
                    <Col sm={12}>
                        <div className="content">
                            {this.props.children}
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
};

export default Main