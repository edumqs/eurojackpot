import React from 'react';

import Eurojackpot from './eurojackpot/Eurojackpot.react';

class Homepage extends React.Component {

    render (){
        return (
            <div className="homepage">
                <Eurojackpot />
            </div>
        );
    }
};

export default Homepage