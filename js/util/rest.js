const $ = window.$ || require('jquery');

export function get(url, data){
    return $.ajax({
        dataType: 'jsonp', //have to use jsonp as this is a cross domain request
        url: url,
        data: JSON.stringify(data),
        method: 'GET'
    });
}