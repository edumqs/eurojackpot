# Eurojackpot results #

### DEMO? ###

-Yes, hosted on my raspberry pi :) **http://edupi2.noip.me:9000/eurojackpot/**

### How do I get set up? ###

1. Clone this repository
2. **cd ../path-to-this-repo**
3. **npm install** (installs all the required dependencies)

### How to run the application in development mode? ###

1. **cd ../path-to-this-repo**
2. **npm run start**
3. Go to your browser and enter http://localhost:9000/ (make sure you don't have anything running on this port)

### How to I build for production? ###

1. **cd ../path-to-this-repo**
2. **npm run webpack-prod**
3. **The generated files will be under /build**
4. **To test this build run 'npm run httpserver' and go to http://localhost:9000/**

### How do I run the tests? ###

1. **Dev mode: npm run watch-test**
2. **Single run: npm run test**